# docker-fedora-distcc

This repository provides a Dockerfile to create a container image for distributed building of C/C++ programs on Fedora using distcc.

The image is rebuilt nightly to ensure it always has the latest packages.

development happens on [this Gitea instance](https://git.dotya.ml/wanderer/docker-fedora-distcc)
